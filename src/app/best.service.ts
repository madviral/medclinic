import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class BestService {
  private url = "https://medcenterkaz.herokuapp.com/api/clinic/request/";
  constructor(private http: HttpClient) {}

  createUser(body: any): Observable<any> {
    return this.http.post<any>(this.url, body);
  }
}
