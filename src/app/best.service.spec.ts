import { TestBed } from '@angular/core/testing';

import { BestService } from './best.service';

describe('BestService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BestService = TestBed.get(BestService);
    expect(service).toBeTruthy();
  });
});
