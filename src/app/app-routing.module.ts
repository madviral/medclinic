import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    loadChildren: () => import("./main/main.module").then(m => m.MainModule)
  },
  {
    path: "registration",
    loadChildren: () =>
      import("./registration/registration.module").then(
        m => m.RegistrationModule
      )
  },
  {
    path: "doctor-list",
    loadChildren: () =>
      import("./doctor/doctor.module").then(m => m.DoctorModule)
  },
  {
    path: "success",
    loadChildren: () =>
      import("./success/success.module").then(m => m.SuccessModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
