import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { NgxMaskModule } from "ngx-mask";
import { NgxPageScrollModule } from "ngx-page-scroll";

import { HeaderComponent } from "./header/header.component";
import { FooterComponent } from "./footer/footer.component";
import { UserFormComponent } from "./user-form/user-form.component";

@NgModule({
  imports: [
    CommonModule,
    NgxMaskModule,
    NgxPageScrollModule,
    ReactiveFormsModule,
    RouterModule
  ],
  declarations: [HeaderComponent, FooterComponent, UserFormComponent],
  exports: [HeaderComponent, FooterComponent, UserFormComponent]
})
export class SharedModule {}
