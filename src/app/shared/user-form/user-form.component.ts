import { Component, OnInit, OnDestroy } from "@angular/core";
import { FormBuilder, Validators, FormGroup, FormArray } from "@angular/forms";
import { Router, ActivatedRoute, ParamMap } from "@angular/router";
import { Subscription } from "rxjs";

@Component({
  selector: "app-user-form",
  templateUrl: "./user-form.component.html",
  styleUrls: ["./user-form.component.scss"]
})
export class UserFormComponent implements OnInit, OnDestroy {
  familyForm: FormGroup;
  singleForm: FormGroup;
  sub: Subscription;
  query: any;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.familyForm = this.formBuilder.group({
      templates: this.formBuilder.array([this.template])
    });

    this.singleForm = this.formBuilder.group({
      firstName: ["", [Validators.required]],
      lastName: ["", [Validators.required]],
      otchestvo: [""],
      phone: ["", [Validators.required]],
      iin: [
        "",
        [
          Validators.required,
          Validators.maxLength(12),
          Validators.minLength(12)
        ]
      ],
      address: ["", [Validators.required]],
      comment: [""],
      myChoices: new FormArray([])
    });

    this.sub = this.route.queryParamMap.subscribe(
      (data: ParamMap) => (this.query = data["params"])
    );
  }

  get template() {
    return this.formBuilder.group({
      firstName: ["", [Validators.required]],
      lastName: ["", [Validators.required]],
      otchestvo: [""],
      phone: ["", [Validators.required]],
      iin: [
        "",
        [
          Validators.required,
          Validators.maxLength(12),
          Validators.minLength(12)
        ]
      ],
      comment: [""],
      address: ["", [Validators.required]]
    });
  }

  addTemplate() {
    const formArray: FormArray = this.familyForm.get("templates") as FormArray;
    formArray.push(this.template);
  }

  removeTemplate() {
    const formArray: FormArray = this.familyForm.get("templates") as FormArray;
    if (formArray.length > 1) {
      formArray.removeAt(formArray.length - 1);
    }
  }

  onSubmit(form: any) {
    console.log(form);

    localStorage.setItem("dataSource", JSON.stringify(form.value.templates));
    this.router.navigate(["doctor-list"]);
  }

  onFirstSubmit(form: any) {
    console.log(form);
    const obj = [
      {
        firstName: form.value.firstName,
        lastName: form.value.lastName,
        otchestvo: form.value.otchestvo,
        phone: form.value.phone,
        iin: form.value.iin,
        address: form.value.address,
        comment: form.value.comment
      }
    ];
    localStorage.setItem("dataSource", JSON.stringify(obj));
    this.router.navigate(["doctor-list"]);
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
