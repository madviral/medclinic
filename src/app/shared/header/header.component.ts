import {
  Component,
  ChangeDetectionStrategy,
  OnInit,
  OnDestroy
} from "@angular/core";
import { Router, ActivatedRoute, ParamMap } from "@angular/router";
import { Subscription } from "rxjs";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderComponent implements OnInit, OnDestroy {
  page: any;
  sub: Subscription;
  constructor(private router: Router, private route: ActivatedRoute) {}

  ngOnInit() {
    this.sub = this.route.queryParamMap.subscribe(
      (params: ParamMap) => (this.page = params.get("page"))
    );
  }

  nextPage() {
    this.router.navigate(["registration"], {
      queryParams: { user: "self", type: "secondary" }
    });
  }

  myFunction() {
    const element = document.getElementById("myTopnav");
    if (element.className === "header-bottom") {
      element.classList.add("responsive");
      element.classList.remove("header-bottom");
    } else {
      element.classList.add("header-bottom");
      element.classList.remove("responsive");
    }
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
