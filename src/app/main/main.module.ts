import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { Ng2CustomCarouselModule } from "ng2-custom-carousel";

import { MainComponent } from "./main.component";
import { SharedModule } from "../shared/shared.module";

@NgModule({
  declarations: [MainComponent],
  imports: [
    CommonModule,
    Ng2CustomCarouselModule,
    SharedModule,
    RouterModule.forChild([
      {
        path: "",
        component: MainComponent,
        pathMatch: "full"
      }
    ])
  ]
})
export class MainModule {}
