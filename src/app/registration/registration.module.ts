import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { ReactiveFormsModule } from "@angular/forms";
import { NgxMaskModule } from "ngx-mask";

import { RegistrationComponent } from "./registration.component";
import { SharedModule } from "../shared/shared.module";

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    RouterModule.forChild([
      {
        path: "",
        component: RegistrationComponent,
        pathMatch: "full"
      }
    ])
  ],
  declarations: [RegistrationComponent]
})
export class RegistrationModule {}
