import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";

import { SuccessComponent } from "./success.component";
import { SharedModule } from "../shared/shared.module";

@NgModule({
  declarations: [SuccessComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([
      {
        path: "",
        component: SuccessComponent,
        pathMatch: "full"
      }
    ])
  ]
})
export class SuccessModule {}
