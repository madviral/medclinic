import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { ReactiveFormsModule } from "@angular/forms";
import { NgxLoadingModule } from "ngx-loading";

import { DoctorComponent } from "./doctor.component";

@NgModule({
  declarations: [DoctorComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NgxLoadingModule,
    RouterModule.forChild([
      {
        path: "",
        component: DoctorComponent,
        pathMatch: "full"
      }
    ])
  ]
})
export class DoctorModule {}
