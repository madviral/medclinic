import { Component, OnInit } from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormArray,
  FormControl
} from "@angular/forms";
import { Subscription } from "rxjs";
import { ActivatedRoute, ParamMap, Router } from "@angular/router";
import { BestService } from "../best.service";

@Component({
  selector: "app-doctor",
  templateUrl: "./doctor.component.html",
  styleUrls: ["./doctor.component.scss"]
})
export class DoctorComponent implements OnInit {
  form: FormGroup;
  sub: Subscription;
  items = [
    {
      image: "pediatr",
      text: "Педиатр",
      name: "a1"
    },
    {
      image: "terapevt",
      text: "Терапевт",
      name: "a2"
    },
    {
      image: "ginekolog",
      text: "Гинеколог",
      name: "a3"
    },
    {
      image: "dermatolog",
      text: "Дерматолог",
      name: "a4"
    },
    {
      image: "nevrolog",
      text: "Невролог",
      name: "a5"
    },
    {
      image: "kardiolog",
      text: "Кардиолог",
      name: "a6"
    },
    {
      image: "hirurg",
      text: "Хирург-травмотолог",
      name: "a7"
    },
    {
      image: "endokrenolog",
      text: "Эндокринолог",
      name: "a8"
    },
    {
      image: "urolog",
      text: "Уролог",
      name: "a9"
    },
    {
      image: "pulmonolog",
      text: "Пульмонолог",
      name: "a10"
    },
    {
      image: "stomatolog",
      text: "Стоматолог",
      name: "a11"
    },
    {
      image: "oftalmolog",
      text: "Офтальмолог",
      name: "a12"
    },
    {
      image: "last",
      text: "Оториноларинголог",
      name: "a13"
    }
  ];
  loading = false;
  lclStorage: any[];

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private bestService: BestService
  ) {}

  ngOnInit() {
    this.form = this.fb.group({
      doctor: ["", [Validators.required]],
      myChoices: new FormArray([])
    });

    this.lclStorage = JSON.parse(localStorage.getItem("dataSource"));
    console.log(this.lclStorage);
  }

  onCheckChange(event: any) {
    const formArray: FormArray = this.form.get("myChoices") as FormArray;
    if (event.target.checked) {
      formArray.push(new FormControl(event.target.value));
    } else {
      let i = 0;
      formArray.controls.forEach((ctrl: FormControl) => {
        if (ctrl.value === event.target.value) {
          formArray.removeAt(i);
          return;
        }
        i++;
      });
    }
  }

  onSubmit(form: any) {
    this.loading = true;
    const body = this.lclStorage.map(item => {
      return {
        name: item.firstName,
        lastname: item.lastName,
        otchestvo: item.otchestvo,
        telephone: item.phone,
        iin: item.iin,
        address: item.address,
        comment: item.comment,
        doctors: this.form.get("myChoices").value
      };
    });
    console.log(body);

    this.bestService.createUser(body).subscribe((value: any) => {
      this.loading = false;
      if (value && value.length >= 1) {
        return this.router.navigate(["success"]);
      }
    });
  }
}
